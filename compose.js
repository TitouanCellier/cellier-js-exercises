const { assert } = require("chai");

const increment = x => x + 1;
const double = y => y * 2;

function compose(fun1, fun2) {
    return value => fun2(fun1(value));
}

const timesTwoPlusOne = compose(increment, double);
assert.deepEqual(timesTwoPlusOne(5), 12);
assert.deepEqual(timesTwoPlusOne(2), 6);
