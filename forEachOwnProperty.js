const assert = require("assert");

Object.prototype.forEachOwnProperty = function(func) {
    
    for (let value of Object.keys(this)) {
        func(value);
    }
}

const o1 = { a: 1 };
const o2 = Object.create(o1);
o2.b = 2;
o2.c = 3;

const props = [];
o2.forEachOwnProperty(prop => props.push(prop));
assert.deepEqual(props, ["b", "c"])