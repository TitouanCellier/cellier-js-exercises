function startsWithHomeMade(str, start) {
    if (str.substr(0, start.length) === start) return true
    return false
}

console.log(startsWithHomeMade("La vie c'est comme une boite de chocolat", "La"))
console.log(startsWithHomeMade("La vie c'est comme une boite de chocolat", "vie"))