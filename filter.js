const {assert} = require("chai");

/**
 * @param {Iterable} iterable Chaîne de caractère, Array, Map, Set
 * @param {function} f Fonction qui transforme chaque élément.
 */
 function filter(iterable, f) {
    let iterator = iterable[Symbol.iterator]();
    return {
        next () {
            let result = iterator.next();
            while (!f(result.value) && !result.done) result = iterator.next();
            return { done: result.done, value : result.done ? undefined : result.value }
        }
    }
}

const iterator = filter("hello", v => "aeiouy".includes(v));
assert.deepEqual(iterator.next().value, "e");
assert.deepEqual(iterator.next().value, "o");
iterator.next().done; // => true