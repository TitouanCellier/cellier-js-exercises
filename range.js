const { assert } = require("chai");

class Range {

    constructor(from, to) {
        this.from = from;
        this.to = to;
    } 

    includes(x) {
        return (x >= this.from && x <= this.to);
    }

    toString() {
        return (`${this.from}...${this.to}`);
    }

    static parse(s) {
        s = s.match(Range.integerRangePattern);
        return new Range (s[1], s[2]);
    }

    static integerRangePattern = /([0-9]+)[.]{3}([0-9]+)/;
}

class Span extends Range {
    constructor(from, span) {
        super(...[from, from + span].sort((a, b) => a - b))
    }
}

let a = new Range(1, 10);
let b = new Span(12, -8);
assert.deepEqual(a.includes(5), true);
assert.deepEqual(a.includes(20), false);
assert.deepEqual(a.toString(), "1...10");
console.log(Range.parse("2...51"));
