const { assert } = require("chai");

function splitHomeMade(str, sep) {
    let word = "";
    let words = [];
    let i = 0;

    while (i <= str.length) {
        if (str.substr(i, sep.length) === sep || i === str.length) {
            words.push(word);
            word = ""
            i += sep.length
        } else {
            word += str[i];
            i++;
        }
    }
    return words
}

assert.deepEqual(splitHomeMade("La vie c'est comme une boite de chocolat", "comme "), ["La vie c'est ", "une boite de chocolat"])
assert.deepEqual(splitHomeMade("La vie c'est comme une boite de chocolat", " "), ["La", "vie", "c'est", "comme", "une", "boite", "de", "chocolat"])