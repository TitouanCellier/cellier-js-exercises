const { assert } = require("chai");

function countainsHomeMade(haystack, needle, startIndex = 0) {
    let indice;
    for (let i = startIndex; i < haystack.length; i++) {
        if (haystack.substr(i, needle.length) === needle) return i;
    } 
    return -1;
}

//console.log(countainsHomeMade("La vie c'est comme une boite de chocolat", "comme"))
assert.deepEqual(countainsHomeMade("La vie c'est comme une boite de chocolat", "La"), 0)
assert.deepEqual(countainsHomeMade("La vie c'est comme une boite de chocolat", "vie", 3), 3)
assert.deepEqual(countainsHomeMade("La vie c'est comme une boite de chocolat", "come", 5), -1)