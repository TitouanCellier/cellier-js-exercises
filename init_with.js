const assert = require("assert");

function initWith(size, f) {
    let arr = [];

    for (let i = 0; i < size; i++) {
        arr.push(f(i));
    }

    return arr;
}

const withZero = () => 0;
const fromZero = index => index;
const from42 = index => 42 + index;
assert.deepEqual(initWith(5, withZero), [0, 0, 0, 0, 0])
assert.deepEqual(initWith(5, fromZero), [0, 1, 2, 3, 4])
assert.deepEqual(initWith(5, from42), [42, 43, 44, 45, 46])