const assert = require("assert");

function restrict(target, keep) {
    let keepKeys = Object.keys(keep);
    let targetKeys = Object.keys(target);
    
    let suppr = targetKeys.find(key => {
        keepKeys.some(value => key = value);
    });

    for (key in suppr) {
        delete target[key];
    }

    return toRestrict;
}

const config = { user: "user", pass: "pass" };
const tooMuchConfig = { vars: "LOG=info", user: "user", pass: "pass", env: "prod" };
assert.deepEqual("vars" in tooMuchConfig, true);
assert.deepEqual("env" in tooMuchConfig, true);

const properConfig = restrict(tooMuchConfig, config);
assert.deepEqual(properConfig === config, false);
assert.deepEqual("vars" in properConfig, false);
assert.deepEqual("env" in properConfig, false);