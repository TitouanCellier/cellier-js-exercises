function hanoi(nb, begin, end) {
    if (nb === 1) console.log(begin, " -> ", end);
    else {
        let other = 6 - (begin + end);
        hanoi(nb - 1, begin, other);
        console.log(begin, " -> ", end);
        hanoi(nb - 1, other, end);
    }
}

hanoi(2, 1, 3);