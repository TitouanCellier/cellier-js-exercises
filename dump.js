const assert = require("assert");

function dump(object) {
    str = `{`;
    
    for (let [key, value] of Object.entries(object)) {
        str += `${key}:` + `${value}`;
    }
    
    str = str.slice(0, str.length - 1);
    str += `}`;
    return str;
}

let obj = {};
obj.firstname = "Alan";
obj.lastname = "Turing";
obj.birthday = [1921, 6, 23];

assert.deepEqual(dump(obj), `{firstname:"Alan",lastname:"Turing",birthday:[1921,6,23]}`)