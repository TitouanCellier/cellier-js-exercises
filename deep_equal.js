const assert = require("assert");

function deepEqual(a1, a2) {

    if (a1.length !== a2.length) {
        return false;
    }

    for (let i = 0; i < a1.length; i++) {
        let e1 = a1[i];
        let e2 = a2[i];
        
        if (Array.isArray(e1) && Array.isArray(e2)) {
            if(!deepEqual(e1, e2)) {
                return false; 
            }
        } else if (Array.isArray(e1) || Array.isArray(e2)) {
            return false;
        }
    }

    return true;
}


const a1 = [1, 2, [3, 4], 5];
const a2 = JSON.parse(JSON.stringify(a1));
const a3 = [1, 2, [3], 5];

assert.deepEqual(a1 === a2, false);
assert.deepEqual(deepEqual(a1, a2), true);
assert.deepEqual(deepEqual(a1, a3), false);