const { assert } = require("chai");

function partial() {
    
}

const f = (x, y, z) => x * (y - z);
partial(f, 2)(3, 4) // => -2   <=> (2 * (3 - 4))